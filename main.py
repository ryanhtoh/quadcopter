from flask import Flask, render_template, Response, jsonify
from gps3.agps3threaded import AGPS3mechanism, agps3
import time
import threading
import board
import adafruit_bno055
from camera_pi import Camera # Raspberry Pi camera module (requires picamera package, developed by Miguel Grinberg)

alt = 0

def updateGPS():
    #threading.Timer(0.5, updateGPS).start()
    for new_data in gps_socket:
        if new_data:
            data_stream.unpack(new_data)
            global alt
            if data_stream.alt:
                alt = data_stream.alt

agps = AGPS3mechanism()  # Instantiate AGPS3 Mechanisms
agps.stream_data()  # From localhost (), or other hosts, by example, (host='gps.ddns.net')
agps.run_thread()  # Throttle time to sleep after an empty lookup, default '()' 0.2 two tenths of a second

gps_socket = agps3.GPSDSocket()
data_stream = agps3.DataStream()
gps_socket.connect()
gps_socket.watch()

i2c = board.I2C()
sensor = adafruit_bno055.BNO055_I2C(i2c)

app = Flask(__name__)

t1 = threading.Thread(target=updateGPS, name = 't1')
t1.start()



@app.route("/_info", methods = ['GET'])
def info():
    lat=agps.data_stream.lat
    lon=agps.data_stream.lon
    global alt

    angle = sensor.euler
    timeNow = time.asctime( time.localtime(time.time()) )
    return jsonify(lat=lat, lon=lon, alt=alt, angle=angle, time=timeNow)

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port =80, debug=True, threaded=True)
