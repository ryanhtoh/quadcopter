# From 5/5/2021

![](Capture.PNG)

This is a quadcopter controlled with an onboard Arduino and Raspberry Pi. 
Data is sent from the FreeRTOS enabled Arduino to the RPi using I2C.
An IMU is used to capture Roll, Pitch, and Yaw, either directly with I2C or I2C with the Arduino intermediate.
A GPS device is used to capture Latitude, Longitude, and Altitude.
A webcam is used to capture video.

All data is streamed to/from a http web-server over TCP/IP.
A webserver was created using Flask (python code), and AJAX, Jquery, html,
and Javascript (html code).

Flask was installed and used to host the web server. Json IMU and GPS data
was periodically sent from Flask to the Flask html template engine called
Jinja2. Picamera (developed by Miguel Grinberg) was installed from
https://github.com/Mjrovai/Video-Streaming-withFlask/blob/master/camWebServer/camera_pi.py to handle camera
streaming. The picamera package was placed in the same directory as the
main files. The html file was placed in the templates folder and the jquery
library was placed in the static folder.

The gps3 library was installed to handle GPS data. 
I used the documentation at: https://pypi.org/project/gps3/ and
https://github.com/wadda/gps3 in order to figure it out. I used
multithreading in order to have the gps3 library update GPS data without
interrupting the server.

The IMU data was sent over I2C to the Raspberry Pi. The RPi had its clock
stretched as in the tutorial: https://learn.adafruit.com/circuitpython-on-
raspberrypi-linux/i2c-clock-stretching in order to ensure the data transferred
properly. I used an I2C clock rate of 9 kHz. Setting up the I2C with
Raspberry Pi used the import libraries from the tutorial:
https://learn.adafruit.com/adafruit-bno055-absolute-orientationsensor/python-circuitpython
This code was activated from the SSH shell with the command: python3
main.py

For the Operating System, I used the official Buster OS, the image labeled
“2021-03-04-raspios-buster-armhf,” and imaged onto a 64 GB micro SD
card with Balena Etcher.

The Raspberry Pi was first connected to the home ethernet system, and I
used Putty to SSH into the system (The IP address was obtained from the
router).

The host was my phone, which was also used as an Android hotspot. I gave
and received commands to the Raspberry Pi from my phone using the SSH
shell on the app RaspController. The data output was recorded using the app
XRecorder. A separate phone was used to simultaneously record the drone
flight visually.
